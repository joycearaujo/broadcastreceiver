package com.example.joyce.atividadebroadcasreciver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        HelloReciver helloReciver = new HelloReciver();

        LocalBroadcastManager m = LocalBroadcastManager.getInstance(this);
        m.registerReceiver(helloReciver,new IntentFilter("BINGO"));
    }


}
